from helpers import *

import urllib
import urllib.parse, urllib.request
import requests
import json
import time
import hmac, hashlib

class ApiInterface:
    def __init__(self, api_key=None, secret=None):
        self.api_key = bytearray(api_key, 'utf-8')
        self.secret = bytearray(secret, 'utf-8')

    # CONSIDER: dynamic calls by name -- ie, this class would mostly be for safety
    # Alternatively, many of the methods can be dynamically called, and overridden
    # for some

    ### Direct access
    # TODO: direct access safety

    def public_api_query(self, command, req={}):
        curr_message(f"query {command} {req}")
        req["command"] = command
        urlstr = "https://poloniex.com/public?" + urllib.parse.urlencode(req)

        ret = urllib.request.urlopen(urllib.request.Request(urlstr), timeout=10)
        response = ret.read().decode('utf-8')
        return json.loads(response)

    def api_query(self, command, req={}):
        curr_message(f"query {command} {req}")

        if(self.api_key == None or self.secret == None):
            # TODO: generalized error handling?
            raise Exception("Tried to make a trading API call without credentials. Please provide\
                            an API key and secret in your config file or use only public methods.")
      
        req['command'] = command
        req['nonce'] = int(time.time()*1000)
        post_data = bytes(urllib.parse.urlencode(req), encoding="utf-8")

        sign = hmac.new(self.secret, post_data, hashlib.sha512).hexdigest()
        headers = {
            'Sign': sign,
            'Key': self.api_key
        }
       
        try:
            ret = urllib.request.urlopen(urllib.request.Request('https://poloniex.com/tradingApi', post_data, headers), timeout=10)
        except urllib.error.HTTPError as e:
            log.log_error(f"Request <{command}> with parameters {req} failed with code {e.code}", term=False)
            return {"error": str(e)}
        response = ret.read().decode('utf-8')
        return json.loads(response)


    ### Bot access methods


    ## Public methods

    def get_ticker(self):
        return self.public_api_query("returnTicker")

    def get_24h_volume(self):
        return self.public_api_query("return24hVolume")

    def get_order_book(self, curr_pair="all", depth=100):
        return self.public_api_query("returnOrderBook", {
            "currencyPair": curr_pair,
            "depth": depth
        })

    def get_public_trade_history(self, currency_pair, start=None, end=None):
        req = {}
        if start:
            req["start"] = start
            # 'end' useless without 'start'
            if end:
                req["end"] = end
            else:
                req["end"] = 9999999999
                    
        return self.public_api_query("returnTradeHistory", req)

    def get_currencies(self):
        return self.public_api_query("returnCurrencies")

    def get_loan_offers(self, curr):
        return self.public_api_query("returnLoanOrders", {"currency": curr})
    
    def get_candlestick_data(self, curr_pair, period=15, duration=30, end=None):
        # Get price history for a currency
        current_time = time.time()
            
        if end and end < current_time:
            start = end - (duration * 60 * 60 * 24)
        else:
            end = 9999999999
            start = current_time - (duration * 60 * 60 * 24)

        data = self.api.public_api_query("returnChartData", {
            "currencyPair": curr_pair,
            "period": period * 60,
            "start": int(start),
            "end": int(end) 
        })

        return data


    ## Private methods

    def get_balances(self):
        return self.api_query("returnBalances")

    def get_complete_balances(self):
        return self.api_query("returnCompleteBalances")

    def get_tradable_balances(self):
        return self.api_query("returnTradableBalances")

    def get_available_account_balances(self, account=None):
        req = {}
        if account:
            req["account"] = account

        return self.api_query("returnAvailableAccountBalances", req)
    
    def get_orders(self, curr_pair="all"):
        # Get orders for a currency, or all currencies if none is provided
        return self.api_query("returnOpenOrders", {"currencyPair": curr_pair})

    def get_order_trades(self, order_number):
        return self.api_query("returnOrderTrades", {"orderNumber": order_number})

    def get_order_status(self, order_number):
        return self.api_query("returnOrderStatus", {"orderNumber": order_number})

    def get_trade_history(self, curr_pair="all", start=None, end=None, limit="10000"):
        req = {"currencyPair": curr_pair, "limit": limit}
        if start:
            req["start"] = start
            if end:
                req["end"] = end
            else:
                req["end"] = 9999999999

        return self.api_query("returnTradeHistory")
    
    def place_order(self, order_type, curr, amount, rate, base_curr="BTC", optional=None, dryrun=False):
        # Get account balance for a currency, or all currencies if None is provided
        # TODO: clientOrderId
        if order_type not in ["buy", "sell", "marginBuy", "marginSell"]:
            raise Exception("Tried to place order of invalid type '" + str(order_type) +\
                            ". Accepted types are: buy|sell|marginBuy|marginSell")

        pair = base_curr + "_" + curr

        req = {
            "currencyPair": pair,
            "amount": nformat(amount),
            "rate": nformat(rate)
        }

        # Only accept valid inputs for optional
        if optional in ["fillOrKill", "immediateOrCancel", "postOnly"]:
            req[optional] = 1
        if dryrun:
            return {"success": "1"}            
        resp = self.api_query(order_type, req)
        if "error" in resp:
            log.log_error("Request " + order_type + " " + str(req) + " failed with error: " + resp["error"])
        return resp

    def cancel_order(self, order_number):
        return self.api_query("cancelOrder", {"orderNumber": order_number})

    def cancel_all_orders(self, curr_pair=None):
        """
        returns:
        success      -- A boolean indication of the success or failure of this operation.
        message	     -- A human readable description of the result of the action.
        orderNumbers -- array of orderNumbers representing the orders that were canceled.
        """
        req = {}
        if curr_pair:
            req["currencyPair"] = curr_pair

        return self.api_query("cancelAllOrders", req)

    def move_order(self, order_number, rate, amount=None):
        req = {
            "orderNumber": order_number,
            "rate": rate
        }

        if amount:
            req["amount"] = amount

        return self.api_query("moveOrder", req)

    def get_margin_summary(self):
        return self.api_query("returnMarginAccountSummary")

    def get_margin_position(self, curr_pair="all"):
        return self.api_query("getMarginPosition", {"currencyPair": curr_pair})
    
    def close_margin_position(self, curr_pair):
        return self.api_query("closeMarginPosition", {"currencyPair": curr_pair})
