cred_file = "cred.txt"

interfaces = {
    "backtesting": {
        "type": "backtest",
        "data": ("candlestick", 5, 30),
        "currencies": ["DOGE", "ETH_ZRX"],
        "simulation": {
            "balance": {"BTC": 1.0, "ETH": 1.0},
            "fee": 0.001
        }
    },
    "simulated": {
        "type": "simulate",
        "currencies": ["CLAM", "MAID", "FLDC", "XRP"],
        "simulation": {
            "balance": {"BTC": 1.0},
            "fee": 0.002
        }
    },
    "live": {
        "update_delay": 5,
        "type": "trade",
        "print_errors": False,
    }
}

active_interface = "live"

bots = [
    {
        "name": "bot1",
        "strategy": "dynamic_margin",
        "currencies": ["BTC"],
        
        #########thinktank
        # "currencies": {
        #     "BTC": ["ATOM", "TRX"],
        #     "USDT": ["BTC"]
        # },

        # "chunk_sizes": {
        #     "BTC": 0.0101,
        #     "USDT": 100.1
        # }

        ###########


        # "chunk_size": 0.000105,
        # "currencies": ["DOGE"],

        # Override settings from strategy
        # TODO?: warn user when using this -- strategy can break
        "strat_override": {
            "base_currency": "USDT",
            "chunk_size": 100.1
        },
        
        "strat_params": {
            "targets": {
                "buy": 0.01,     # buy target base
                "sell": 0.01,     # sell target base
                "size": 0.002,   # How much active_chunks affects target
                "streak": 0.0025,  # how much doing several of the same trade in a row affects target
                "open": 0.00,     # How much more is required to open a new position
                "target_margin": 0.75,  # Margin % to aim for, considered safe
                "panic": 0.35,
                "danger": 0.6,    # At margin % below this, every extension will be at max_target
                "max_target": 0.2,    # Maximum target % 
                "min_target": 0.015
            },
            "dry": False
        }
    }
]
