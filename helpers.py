import logger

# TODO: Doubled references to colors. Remove the individual variables
# when deemed safe

TEXT_RED =     "\u001b[31m"
TEXT_GREEN =   "\u001b[32m"
TEXT_YELLOW =  "\u001b[33m"
TEXT_BLUE =    "\u001b[34m"
TEXT_MAGENTA = "\u001b[35m"
TEXT_CYAN =    "\u001b[36m"
TEXT_WHITE =   "\u001b[37m"
TEXT_LIGHT_RED =     "\u001b[91m"
TEXT_LIGHT_GREEN =   "\u001b[92m"
TEXT_LIGHT_YELLOW =  "\u001b[93m"
TEXT_LIGHT_BLUE =    "\u001b[94m"
TEXT_LIGHT_MAGENTA = "\u001b[95m"
TEXT_LIGHT_CYAN =    "\u001b[96m"
TEXT_LIGHT_WHITE =   "\u001b[97m"
TEXT_END = "\u001b[0m"
LINE_END = "\033[K\n"

COLOR_CODES = {
    "RED":     "\u001b[31m",
    "GREEN":   "\u001b[32m",
    "YELLOW":  "\u001b[33m",
    "BLUE":    "\u001b[34m",
    "MAGENTA": "\u001b[35m",
    "CYAN":    "\u001b[36m",
    "WHITE":   "\u001b[37m",
    "LIGHT_RED":     "\u001b[91m",
    "LIGHT_GREEN":   "\u001b[92m",
    "LIGHT_YELLOW":  "\u001b[93m",
    "LIGHT_BLUE":    "\u001b[94m",
    "LIGHT_MAGENTA": "\u001b[95m",
    "LIGHT_CYAN":    "\u001b[96m",
    "LIGHT_WHITE":   "\u001b[97m",
}

log = logger.Logger()
curr_message = lambda s: log.log_message("logs/current.txt", s, print_to_term=False)

def color_string(text, color):
    color = color.upper()
    if color in COLOR_CODES:
        return COLOR_CODES[color] + str(text) + TEXT_END
    return text

def nformat(n):
    return "{:.8f}".format(float(n))

def read_conf(conf, name, default=None):
    if name in conf.keys():
        return conf[name]
    else:
        return default

