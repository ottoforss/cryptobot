# from simulated_api import SimulatedApi
from threading import Thread
import time

from api_interface import ApiInterface
from helpers import *

class InterfaceManager(Thread):
    def __init__(self, conf, cred):
        Thread.__init__(self)

        self.update_delay = read_conf(conf, "update_delay", 5)
        self.reserved = read_conf(conf, "reserved", 0.0)
        self.print_errors = read_conf(conf, "print_errors", False)

        # TODO: For now, default status message is status for margin account --
        # which doesn't make sense e.g. if you're spot trading. You can still
        # define your own message so it can be circumvented.
        self.status_message = read_conf(conf, "status_message", self.marg_status_msg)
        
        # Add the properties of the correct interface type to this instance
        self.type = read_conf(conf, "type", "simulate")

        # if(self.type in ["simulate", "backtest"]):
        #     sim_conf = read_conf(conf, "simulation", {})
        #     self.api = SimulatedApi(sim_conf)
        # elif(self.type == "trade"):
        self.api = ApiInterface(cred["api_key"], cred["secret"])
        # else:
        #     raise Exception("Invalid interface type. Accepted values are: simulate|backtest|trade")

        #####################

        self.latest_update = time.time()
        self.latest_orders = {}

        self.update_balances()
        self.update_margin_positions()
        
        self.pending_actions = []

        self.bots = []
        self.bot_orders = {}
        self.currency_pairs = []

        self.counter = 0

        # Set to false when the run function should stop
        self.active = True

    def connect_bot(self, bot):
        self.bots.append(bot)
        self.bot_orders[bot] = [] 
        self.currency_pairs = set(self.currency_pairs + 
                          [bot.base_currency + "_" + x for x in bot.currencies])
        self.update(ignore_bots=True)
        bot.start()

    def update_orders(self):
        orders = self.api.get_orders()
        self.latest_orders = { int(x["orderNumber"]): x for x in orders }

    def update_balances(self):
        self.balances = self.api.get_balances()

    def update_margin_positions(self):
        self.margin_account = self.api.get_margin_summary()
        self.margin_positions = self.api.get_margin_position()

    def order_status(self, order_list):
        """
        Fetches the status of a list of orders
        """
        # TODO: is this really how this function should work?
        self.update_orders()
        
        out = {}
        for e in order_list:
            if e in self.latest_orders:
                out[e] = self.latest_orders[e]
            else:
                out[e] = None

        return out
    
    def marg_status_msg(self):
        marg = self.get_margin_account()

        out_s = ""
        out_s += f"\n{time.strftime('%Y-%m-%d %H:%M:%S - ')}#{self.counter}\n"
        out_s += f'Value:  { marg["totalValue"] }\n'
        out_s += f'P/L:    { format_pl(marg["pl"]) }\n'
        out_s += f'Total:  { marg["netValue"] }\n'
        out_s += f'Margin: { round(100 * float(marg["currentMargin"]), 2) }%\n\n'

        return out_s

    def get_balance(self, curr):
        if not curr in self.balances:
            return 0
        return float(self.balances[curr])

    def get_margin_position(self, curr_pair):
        if curr_pair in self.margin_positions:
            return self.margin_positions[curr_pair]
        return {
            'type': 'none',
            'amount': '0.00000000',
            'total': '0.00000000',
            'basePrice': '0.00000000',
            'liquidationPrice': -1,
            'pl': '0.00000000',
            'lendingFees': '0.00000000'
        }

    def get_margin_account(self):
        return self.request_action(self, "get_margin_summary")

    def request_action(self, source, api_function, *params):
        if not hasattr(self.api, api_function):
            log.log_error("API has no function " + api_function + ", or it hasn't been implemented. " +
                             "Aborting request")
            return False

        resp = getattr(self.api, api_function)(*params)

        if "orderNumber" in resp:
            self.bot_orders[source].append(resp["orderNumber"])
        
        return resp
        
    def update(self, ignore_bots=False):
        self.counter += 1
        update_time = time.time()

        update_data = {"time": update_time}

        self.update_balances()
        self.update_margin_positions()
        
        ticker = self.api.get_ticker()
        order_book = self.api.get_order_book()
        volumes = self.api.get_24h_volume()
        
        orders = self.api.get_orders()

        for curr in self.currency_pairs:
            update_data[curr] = {
                "24h_volume": volumes[curr],
                "ticker": ticker[curr],
                "trades": self.api.get_public_trade_history(curr),
                "asks": order_book[curr]["asks"],
                "bids": order_book[curr]["bids"],
                "orders": orders[curr],
            }

        for bot in self.bots:
            bot.status.data = update_data
            if bot.status.active and not ignore_bots:
                bot.update()
        
        log.generate_status(self)

        self.latest_update = update_time

    def run(self):
        while self.active:
            curr_message("Starting run")
            update_start = time.time()
            try:
                self.update()
            except Exception as e:
                log.log_error(f"Encountered error: {type(e)} {e}", term=self.print_errors)
            time_elapsed = time.time() - update_start
            if self.update_delay - time_elapsed > 0:
                time.sleep(self.update_delay - time_elapsed)
            # TEMPORARY
            # clear current.txt before moving to the next one
            open('logs/current.txt', "w").close()

