import json
import time
import traceback

def time_format(fstr=f"%Y-%m-%d %H:%M:%S - "):
    return time.strftime(fstr)

class Logger:
    def __init__(self, bot=None, log_dir="logs"):
        self.bot = bot
        self.log_dir = log_dir

    def save_status(self):
        save_dict = {"status": {}}
        for field in ["to_save", "currencies"]:
            save_dict[field] = self.bot.__dict__[field]
        for field in self.bot.to_save:
            save_dict["status"][field] = self.bot.status.__dict__[field]

        with open(f"{self.log_dir}/{self.bot.name}.json", "w") as f:
            json.dump(save_dict, f, indent=2)

    def load_status(self):
        path = f"{self.log_dir}/{self.bot.name}.json"
        try:
            with open(path, "r") as f:
                result = json.load(f)
            
            self.bot.status.saved = result
            for k, v in result["status"].items():
                self.bot.status.__dict__[k] = v
        except Exception as e:
            self.log_error(f"Failed to load status: {type(e)} - {e}", term=False)
            return False

        self.log_action("Successfully loaded status from " + path, term=False)
        return result

    def generate_status(self, mng, skip_print=False):
        full_status = ""
        if(callable(mng.status_message)):
            full_status += mng.status_message(mng)
        for bot in mng.bots:
            if(callable(bot.status_message)):
                full_status += bot.status_message(bot.status)
        self.status_message(full_status.strip(), skip_print)

    def status_message(self, message, skip_print=False):
        rows = message.count("\n")
        
        self.log_message(f"{self.log_dir}/status.txt", message, mode="w", append_time=False, print_to_term=False)
        # Clear rest of line at newline
        message = message.replace("\n", "\033[K\n")
        # end moves cursor up 'rows' rows
        if not skip_print:
            print(message, end=f"\033[{rows}F")

    def log_action(self, message, term=True):
        # TODO: save action data in addition to message text, for history analysis etc.
        self.log_message(f"{self.log_dir}/actions.txt", message, print_to_term=term)
    
    def log_error(self, message, term=False):
        if self.bot:
            self.bot.status.errors += 1
        self.log_message(f"{self.log_dir}/errors.txt", message, print_to_term=term)
        self.log_message(f"{self.log_dir}/last_error.txt", traceback.format_exc(), mode ="w", print_to_term=False)

    def log_message(self, file_path, message, mode="a", append_time=True, print_to_term=True):
        if append_time:
            message = time_format() + message
        with open(file_path, mode) as f:
            f.write(message + "\n")
        if print_to_term:
            print(message)