**TODOS**

* write a working bot script
handle fetching balances in the interface manager



### API connection

Accessing the API

api_interface: accesses a specific API (starting w/ only Poloniex)
?: not necessary for only polo

*variables*
- credentials
- ? list available functionality for exchange

**2 methods for direct access. These only grab data and are for the rest of the api interface and for someone who wants full control of that particular api:**
* **public_api_query()**: public queries without credentials
* **api_query()**: queries that require an account (ie api credentials)

**generalized methods to translate api queries for interfacing with the rest of the program. Abstracts away some things that are specific to that exchange and (? possibly) converts data/changes some names to be unified. Still only sends requests and receives resonses, does not try to do anything fancy.**
_The rest of the program should only interface with these methods! (except direct access, ofc)_

**TODO**
_public_
- trade history
- volume
- currencies/currency pairs
- order book

_private_
- return_balances()
- Orders: list, status, cancel, move
- trade: buy/sell
- margin: buy/sell, list positions, close
- history





*: implemented
!: will not implement

- * Public HTTP API Methods
-     * returnTicker
-     * return24hVolume
-     * returnOrderBook
-     * returnTradeHistory (public)
-     * returnChartData (as get_candlestick_data)
-     * returnCurrencies
-     * returnLoanOrders

- Private HTTP API Methods
-     * returnBalances
-     * returnCompleteBalances
-     ! returnDepositAddresses
-     ! generateNewAddress
-     returnDepositsWithdrawals
-     * returnOpenOrders
-     * returnTradeHistory (private)
-     * returnOrderTrades
-     * returnOrderStatus
-     * buy   (in place_order)
-     * sell  (in place_order)
-     * cancelOrder
-     * cancelAllOrders
-     * moveOrder
-     ! withdraw
-     returnFeeInfo
-     * returnAvailableAccountBalances
-     * returnTradableBalances
-     transferBalance
-     * returnMarginAccountSummary
-     * marginBuy  (in place_order)
-     * marginSell  (in place_order)
-     * getMarginPosition
-     * closeMarginPosition
-     createLoanOffer
-     cancelLoanOffer
-     returnOpenLoanOffers
-     returnActiveLoans
-     returnLendingHistory
-     toggleAutoRenew

### Unit testing

In order of implementation? Unit tests where possible. May need integration tests for certain parts.

*: completed

- Bot
-   configs get defined and have working default values
-   config functions (on_init etc.) work
-   place_order does correct things! (Needs mock API interface?)
-       sends action request to manager
-       calculates chunk size/volumes correctly
-       logs message through logger

- * Helpers

- Logger
-   * successfully saves and loads state of bot
-   logs errors and actions
-   * logs status message of multiple bots simultaneously without causing issues

- InterfaceManager
-    bunch o' stuff ?????

- ApiInterface/simulated API
-   should handle errors more robustly

- Strategies (integration test??)



### Websocket

Websocket should allow faster, more reactive trading.

**TODO**

- Websocket API
-     Requests and Responses
-     Subscribing and Unsubscribing
-     Channel Descriptions
-     Account Notifications (Beta)
-     Ticker Data
-     24 Hour Exchange Volume
-     Heartbeats
-     Price Aggregated Book