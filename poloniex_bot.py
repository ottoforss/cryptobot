import time
import json
import types
import importlib
from functools import partial

import api_interface 
import logger
from helpers import *


class Bot:
    def __init__(self, manager, conf):
        self.manager = manager
        self._call = lambda f: f(self.status) if callable(f) else None

        strat = read_conf(conf, "strategy", None)
        if strat:
            try:
                # Apply bot config from strategy
                strat = importlib.import_module("strategy." + strat)
                conf.update(strat.conf)
            except ModuleNotFoundError:
                strat = None

        if strat:
            # Apply strategy parameters from config
            params = read_conf(conf, "strat_params", {})
            strat.params.update(params)

            # Override bot config from strategy
            override = read_conf(conf, "strat_override", {})
            conf.update(override)
            
        # The name/id of the bot. This should be unique for each bot
        self.name = read_conf(conf, "name", None)
        # The currency that the bot trades from. All configs using fixed values
        # of a currency refers to this.
        self.base_currency = read_conf(conf, "base_currency", "BTC")
        # The currencies that the bot interacts with
        self.currencies = read_conf(conf, "currencies", [])
        # Budget assigned to bot. Uses everything available if None
        self.chunk_size = read_conf(conf, "chunk_size", None)
        # How many chunks the assigned currency should be split into
        self.chunks = read_conf(conf, "chunks", 10)
        # Trading type: spot|margin
        # CONSIDER: lending
        self.trade_type = read_conf(conf, "trade_type", "spot")
        # Define what fields of status should be saved
        self.to_save = read_conf(conf, "save", [])

        # Function to run when bot is initialized
        self.on_init = read_conf(conf, "on_init", None)
        # Function to run when bot starts
        self.on_start = read_conf(conf, "on_start", None)
        # Function to run when bot updates
        self.on_update = read_conf(conf, "on_update", None)
        # Function to run when bot stops
        self.on_close = read_conf(conf, "on_close", None)
        # Function to generate a status message for the bot (output)
        self.status_message = read_conf(conf, "status_message", None)

        # Status of bot. Generic object to store data that gets passed to functions etc.
        self.status = types.SimpleNamespace()
        self.status.active = False

        # Make buy and sell functions available in bot script
        if self.trade_type == "spot":
            self.status.buy = partial(self.place_order, "buy")
            self.status.sell = partial(self.place_order, "sell")
        elif self.trade_type == "margin":
            self.status.buy = partial(self.place_order, "marginBuy")
            self.status.sell = partial(self.place_order, "marginSell")
        else:
            raise Exception("Invalid trade_type for bot " + str(self.name) + ". Valid values are: " +
                            "spot|margin")

        self.status.currency_pairs = [self.base_currency + "_" + c for c in self.currencies]

        # Direct access to API
        self.status.api = self.manager.api

        self.request_action = partial(self.manager.request_action, self)
        
        self.active_chunks = { cp: 0 for cp in self.status.currency_pairs }
        
        self.status.errors = 0
        self.status.counter = 0

        self.status.active_chunks = self.active_chunks
        self.status.currencies = self.currencies

        self.status.manager = self.manager
        self.status.chunk_size = self.chunk_size

        self.logger = logger.Logger(self)

        ignore_saved = read_conf(conf, "ignore_saved_status", False)

        # Load data
        if not ignore_saved:
            for f in self.to_save:
                self.status.__dict__[f] = None
            self.logger.load_status()
        self._call(self.on_init)

    def start(self):
        self._call(self.on_start)            
        self.status.active = True

    def stop(self):
        self._call(self.on_close)
        self.status.active = False

    def update(self):
        if not self.status.active:
            return

        self._call(self.on_update)        
        self.logger.save_status()

        self.status.counter += 1

    def place_order(self, action_type, curr, rate, chunks=1, optional=None, dryrun=False):
        closed = False
        curr_pair = f"{self.base_currency}_{curr}"

        if self.trade_type == "margin":
            mult = -1 if action_type[-2:] == "ll" else 1
            ac = mult * self.active_chunks[curr_pair]
            if ac < 0:
                ac = -ac
                if ac == chunks:
                    resp = self.request_action("close_margin_position", self.base_currency + "_" + curr)
                    amount = sum([float(x["amount"]) for x in resp["resultingTrades"][self.base_currency + "_" + curr]])
                    closed = True
                marg_pos = self.manager.get_margin_position(self.base_currency + "_" + curr)
                amount = abs(float(marg_pos["amount"])) * (min(ac, chunks)/ac) + \
                        (self.chunk_size * -min(ac-chunks, 0))/rate
            else:
                amount = (self.chunk_size * chunks)/rate
        else:
            if action_type == "sell":
                pos = self.manager.get_balance(curr)
                if chunks >= self.active_chunks[curr_pair]:
                    chunks = self.active_chunks[curr_pair]
                amount = pos * chunks/self.active_chunks[curr_pair]
            else:
                amount = (self.chunk_size * chunks)/rate
        
        params = [
            curr,
            amount,
            rate,
            self.base_currency,
            optional,
            dryrun
        ]

        color = "\u001b[92m" if action_type[-2:] == "uy" else "\u001b[91m"
        dry = "[DRY RUN]" if dryrun else ""
        s = (f"{color}{action_type} {dry} {self.base_currency}_{curr} <{nformat(amount)} {curr} at {nformat(rate)}> " +
             f"Total: {nformat(amount*rate)} {self.base_currency}{' (Closed)' if closed else ''}\u001b[0m\033[K")
        
        self.logger.log_action(s, term=True)

        if not closed:
            resp = self.request_action("place_order", action_type, *params)

        if resp: # request success
            if action_type[-2:] == "uy":
                self.active_chunks[curr_pair] += chunks
            else:
                self.active_chunks[curr_pair] -= chunks
            return True

        return False
