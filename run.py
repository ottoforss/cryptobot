from poloniex_bot import Bot
from interface import InterfaceManager
import config
import sys
import contextlib

import signal

if __name__ == "__main__":
    # When the program exits, move cursor to bottom on keyboardinterrupt 
    # and reset echo
    def f(*_):
        # if blocked_input:
        #     termios.tcsetattr(fd, termios.TCSADRAIN, old)
        print("\033[100B")
        exit()
    signal.signal(signal.SIGINT, f)
    blocked_input = False

    with open(config.cred_file, "r") as f:
        cred = dict(zip(["api_key", "secret"], [l.strip() for l in f.readlines()]))

    intf = InterfaceManager(config.interfaces[config.active_interface], cred)
    
    for bconf in config.bots:
        intf.connect_bot(Bot(intf, bconf))


    # Block input echo on unix
    # try:
    #     import termios
    #     fd = sys.stdin.fileno()
    #     old = termios.tcgetattr(fd)
    #     new = old[:]
    #     new[3] = new[3] & ~termios.ECHO
    #     termios.tcsetattr(fd, termios.TCSADRAIN, new)
    #     blocked_input = True
    # finally:
    #     pass

    intf.start()
