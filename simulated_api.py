import time

from api_interface import ApiInterface
from helpers import *

class SimulatedApi:
    def next_order_nr(self):
        n = self.fake_order_nr
        self.fake_order_nr += 1
        return n

    def __init__(self, conf):
        self.balance = {}
        for k,v in read_conf(conf, "balance", {"BTC":1.0}):
            self.set_balance(k, "add", v)
        self.public_api = ApiInterface() # Connect to api without credentials

        # A singular fee for every trade that goes through. There are no
        # takers in the simulation.
        self.fee = read_conf(conf, "fee", 0.002)

        self.fake_order_nr = 0

        self.open_orders = {}
        self.closed_orders = {}

        self.latest_prices = None
        self.latest_update = None


    def public_api_query(self, *params):
        if params[0] == "returnOrderBook":
            data = self.public_api.public_api_query("returnOrderBook")
            return self.update(data, "orderBook")
        elif params[0] == "returnTicker":
            data = self.public_api.public_api_query("returnTicker")
            return self.update(data, "ticker")
        else:
            return self.public_api.public_api_query(*params)

    def api_query(self, command, req={}):
        methods = [
            "returnBalances",
            "returnCompleteBalances",
            "returnOpenOrders",
            "returnTradeHistory",
            # "returnOrderTrades",
            # "returnOrderStatus",
            "buy",
            "sell",
            "cancelOrder",
            "moveOrder"
        ]

        if command in methods:
            getattr(self, command)(req)
        else:
            return {"error": "Command '" + command + "' is not available \
                    in the simulated api. Available methods are:\n" + ", ".join(methods)}

    #### Api methods ####

    def returnBalances(self, req={}):
        return { k:nformat() for k,v in self.balance.items() }

    def returnCompleteBalances(self, req={}):
        return { k:{k2: nformat(v2) for k2,v2 in v.items()} for k,v in self.balance.items() }

    def returnOpenOrders(self, req={}):
        if "currencyPair" in req:
            if req["currencyPair"] in self.open_orders:
                return self.open_orders[req["currencyPair"]]
            else:
                return []

        return self.open_orders

    def returnTradeHistory(self, req={}):
        return self.closed_orders

    def buy(self, req={}):
        return self.place_order(req["currencyPair"], "buy", req["amount"], req["rate"])

    def sell(self, req={}):
        return self.place_order(req["currencyPair"], "sell", req["amount"], req["rate"])

    def cancelOrder(self, req={}):
        if "orderNumber" in req:
            order_nr = int(req["orderNumber"])
        else:
            return {"error": "cancelOrder: No orderNumber supplied"}

        for pair,orders in self.open_orders.items():
            for o in orders:
                if o["orderNumber"] == order_nr:
                    base_curr, curr = pair.split("_")

                    self.open_orders[pair].remove(o)
                    if o["type"] == "buy":
                        self.set_balance(base_curr, "off_order", o["amount"])
                    elif o["type"] == "sell":
                        self.set_balance(curr, "off_order", o["amount"])

                    return {"success": 1}

        return {"error": "cancelOrder: Order could not be found."}

    def moveOrder(self, req={}):
        if "orderNumber" in req:
            order_nr = int(req["orderNumber"])
        else:
            return {"error": "moveOrder: No orderNumber supplied"}

        for pair, orders in self.open_orders.items():
            for o in orders:
                if o["orderNumber"] == order_nr:
                    base_curr, curr = pair.split("_")
                    old_amount = float(o["amount"])

                    if "amount" in req:
                        new_amount = float(req["amount"])
                    else:
                        new_amount = old_amount

                    new_total = new_amount * float(req["rate"])

                    if o["type"] == "buy":
                        if new_total > get_usable_balance(base_curr) + float(o["total"]):
                            return {"error": "Not enough funds to move order."}
                        self.set_balance(base_curr, "off_order", float(o["total"]))
                        self.set_balance(base_curr, "on_order", new_total)                        
                    elif o["type"] == "sell"
                        if new_amount > get_usable_balance(curr) + old_amount:
                            return {"error": "Not enough funds to move order."}
                        self.set_balance(curr, "off_order", old_amount)
                        self.set_balance(curr, "on_order", new_amount)

                    self.open_orders[pair].remove(o)
                    self.open_orders[pair].append({
                        "orderNumber": o["orderNumber"],
                        "type": o["type"],
                        "amount": nformat(new_amount),
                        "rate": nformat(float(req["rate"])),
                        "total": nformat(new_amount*float["rate"])
                    })

                    return {"success": 1, "orderNumber": o["orderNumber"]}

        return {"error": "moveOrder: Order could not be found."}

    #### Private methods #####

    def get_usable_balance(self, curr):
        if not self.balance[curr] or self.balance[curr]["available"]:
            return 0.0
        return self.balance[curr]["available"]

    def set_balance(self, curr, action, amount):
        # No proper exception handling here, just abort the function
        if action not in ["add", "remove", "on_order", "off_order", "delete_from_order"]:
            return

        if not self.balance[curr]:
            self.balance[curr] = {"available": 0.0, "onOrders": 0.0}

        # NOTE: Does not raise exceptions if the full amount cannot be used!
        # TODO: This should be logged as a warning when I get around to the
        # logger
        if action == "add":
            self.balance[curr]["available"] += amount
        elif action == "remove":
            if amount > self.balance[curr]["available"]:
                self.balance[curr]["available"] = 0.0
            else:
                self.balance[curr]["available"] -= amount
        elif action == "on_order":
            if amount > self.balance[curr]["available"]:
                self.balance[curr]["onOrders"] += self.balance[curr]["available"]
                self.balance[curr]["available"] = 0.0
            else:
                self.balance[curr]["onOrders"] += amount
                self.balance[curr]["available"] -= amount
        elif action == "off_order":
            if amount > self.balance[curr]["onOrders"]:
                self.balance[curr]["available"] += self.balance[curr]["onOrders"]
                self.balance[curr]["onOrders"] = 0.0
            else:
                self.balance[curr]["available"] += amount
                self.balance[curr]["onOrders"] -= amount
        elif action == "delete_from_order":
            if amount > self.balance[curr]["onOrders"]:
                self.balance[curr]["onOrders"] = 0.0
            else:
                self.balance[curr]["onOrders"] -= amount

    def place_order(self, curr_pair, order_type, amount, rate):
        try:
            base_curr, curr = curr_pair.split("_")
        except:
            return {"error": "Invalid currency pair " + curr_pair}

        if((order_type == "buy" and self.get_usable_balance(base_curr) < amount * rate) or
           (order_type == "sell" and self.get_usable_balance(curr) < amount)):
            return {"error": "Not enough funds to place order"}

        pair = base_curr + "_" + curr

        if(not self.open_orders[pair]):
            self.open_orders[pair] = []

        order = {
            "orderNumber": self.next_order_nr(),
            "type": order_type,
            "amount": nformat(amount),
            "rate": nformat(rate),
            "total": nformat(amount*rate)
        }

        self.open_orders[pair].append(order)

        if order_type == "buy":
            self.set_balance(base_curr, "on_order", amount * rate)
        elif order_type == "sell":
            self.set_balance(curr, "on_order", amount)

        return {"orderNumber": order["orderNumber"]}

    def backtest_candlesticks(self, data):
        pass

    def update(self, data, data_type="ticker", custom_threshold=None):
        # Update simulation with a single ticker tick, realtime or otherwise
        if callable(custom_threshold):
            get_prices = custom_threshold
        elif data_type == "ticker":
            get_prices = lambda d: (float(d["lowestAsk"]), float(d["highestBid"]))
        elif data_type == "orderBook":
            get_prices = lambda d: (float(d["asks"][0]), float(d["bids"][0]))
        elif data_type == "candlestick":
            get_prices = lambda d: (float(d["high"]), float(d["low"]))
        else:
            return {"error": "Simulated api update: Unable to define a threshold function. " +
                "Use a predefined format (ticker|orderBook|candlestick) or define your own with " +
                "the custom_threshold parameter."}

        self.latest_update = time.time()

        # Mark the orders to be filled so that there's no weirdness with changing
        # lists that are being iterated through
        filled_orders = []

        for pair, orders in self.open_orders.items():
            if pair in data:
                high, low = get_prices(data[pair])

                for o in orders:
                    if (o["type"] == "buy" and float(o["rate"]) > low) or \
                       (o["type"] == "sell" and float(o["rate"]) < high):
                        filled_orders.append((pair,o))

        for pair, order in filled_orders:
            self.open_orders[pair].remove(order)

            base_curr, curr = pair.split("_")

            if(order["type"] == "buy"):
                self.set_balance(base_curr, "delete_from_order", float(order["total"]))
                self.set_balance(curr, "add", float(order["amount"]) * (1-self.fee))
            elif(order["type"] == "sell"):
                self.set_balance(curr, "delete_from_order", float(order["amount"]))
                self.set_balance(base_curr, "add", float(order["total"] * (1-self.fee)))

            order["fee"] = self.fee
            self.closed_orders.append(order)

        return data







