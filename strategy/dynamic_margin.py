from helpers import *
import time

#### Bot strategy

params = {
    "targets": {
        "buy": 0.03,      # buy target base and minimum %
        "sell": 0.035,    # sell target base and minimum %
        "size": 0.003,    # How much active_chunks affects target
        "streak": 0.005,  # how much doing several of the same trade in a row affects target
        "open": 0.05,     # How much more is required to open a new position
        "target_margin": 0.9,  # Margin % to aim for, considered safe
        "danger": 0.5,    # At margin % below this, every extension will be at max_target
        "panic": 0.4,
        "max_target": 0.15,     # Maximum target % 
        "min_target": 0.02      # Minimum target %
    },

    # Dry run (no real trade orders are made if True)
    "dry": True
}

def calc_target(s, active, streak, action):
    false_val = lambda x, val: 0 if x else val
    t = lambda field: params["targets"][field]

    marg_pos = s.manager.get_margin_account()

    marg = float(marg_pos["currentMargin"])

    # Something's fishy, return max until it returns to normalcy
    if marg == 0:
        return t("max_target")

    extend = active >= 0  # This action extends the position
    panic_mode = marg < t("panic")
    
    base = t(action)
    span = t("target_margin") - t("danger")
    # variant is target % before target margin
    variant = max(0.01, base + active * t("size") + streak * t("streak") + false_val(active, t("open")))

    danger_add = t("max_target")/span

    modifier = max(0, (t("target_margin") - marg) * danger_add)
    modifier = modifier if extend else -modifier

    target = variant + modifier
    if not panic_mode:
        target = max(variant, target)
        target = max(t("min_target"), target)
    final =  min(t("max_target"), target)
    
    return final

buy_target  = lambda s, active, streak: calc_target(s, active, streak, "buy")
sell_target = lambda s, active, streak: calc_target(s, -active, streak, "sell")

def format_pl(val):
    c = "LIGHT_RED" if float(val) < 0 else "LIGHT_GREEN"
    return color_string(str(val), c)

def calculate_chunks(s, c):    
    marg_pos = s.manager.get_margin_position(c)
    amnt = float(marg_pos["amount"])
    base = float(marg_pos["basePrice"])
    pl = float(marg_pos["pl"])

    target_profit = s.chunk_size
    chunks_per_extend = 1.5
    profit_step = 3

    below_target = target_profit - pl 
    added = (below_target / s.chunk_size) * profit_step

    strike = s.pairs[c]["strike"]
    s.active_chunks[c] = round((chunks_per_extend * amnt * strike)/s.chunk_size + added * (1 if amnt >= 0 else -1))

def calculate_targets(s, c):
    strike_price = s.pairs[c]["strike"]
    
    s.pairs[c]["target"]["buy"] = round(strike_price  - (buy_target(s, s.active_chunks[c], s.pairs[c]["streak"]["buy"]) * strike_price), 8)
    s.pairs[c]["target"]["sell"] = round(strike_price + (sell_target(s, s.active_chunks[c], s.pairs[c]["streak"]["sell"]) * strike_price), 8)


def on_init(s):
    s.base_cooldown = 10

    s.pairs = s.pairs or {}
    for pair in s.currency_pairs:
        if not pair in s.pairs:
            s.pairs[pair] = {
                "cooldown": s.base_cooldown,
                "strike": None,
                "target": {
                    "buy": None,
                    "sell": None
                },
                "streak": {
                    "buy": 0,
                    "sell": 0
                }
            }

    print("I'm alive!!")

def on_start(s):
    for c in s.currency_pairs:
        ask_price = float(s.data[c]["asks"][0][0])
        bid_price = float(s.data[c]["bids"][0][0])

        for d in s.pairs.values():
            d["cooldown"] = s.base_cooldown

        if not s.pairs[c]["strike"]:
            s.pairs[c]["strike"] = (ask_price + bid_price)/2

        
        calculate_chunks(s, c)
        calculate_targets(s, c)

def on_update(s):
    curr_message("Starting on_update " + str(s.counter))
    for c in s.currency_pairs:
        # TODO: Sooo hacky --- probably should use currency pairs everywhere?
        curr_message(c)
        curr = c.split("_")[-1]
        
        if s.pairs[c]["cooldown"] <= 0:
            ask_price = float(s.data[c]["asks"][0][0])
            bid_price = float(s.data[c]["bids"][0][0])
            placed = False

            targets = s.pairs[c]["target"]
            streaks = s.pairs[c]["streak"]

            # Recalibrate chunks every so often
            if s.counter % 10 == 0:
                calculate_chunks(s, c)
                calculate_targets(s, c)

            if ask_price <= targets["buy"]:
                s.buy(curr, ask_price, dryrun=params["dry"])
                streaks["buy"] += 1
                streaks["sell"] = 0
                strike_price = ask_price
                placed = True
            elif bid_price >= targets["sell"]:
                s.sell(curr, bid_price, dryrun=params["dry"])
                streaks["buy"] = 0
                streaks["sell"] += 1
                strike_price = bid_price
                placed = True

            if placed:
                s.pairs[c]["cooldown"] = s.base_cooldown
                s.pairs[c]["strike"] = strike_price
                calculate_targets(s, c)
        s.pairs[c]["cooldown"] -= 1

def status_message(s):
    out_s = ""
    for c in s.currency_pairs:
        marg_pos = s.manager.get_margin_position(c)

        out_s += f'<{color_string(c, "LIGHT_CYAN")}> {format_pl(marg_pos["pl"])}  b{s.pairs[c]["streak"]["buy"]} s{s.pairs[c]["streak"]["sell"]}' +\
                 f' {s.pairs[c]["cooldown"]} < {TEXT_LIGHT_YELLOW}{s.active_chunks[c]}: {marg_pos["amount"]}{TEXT_END} >\n'
        out_s += f'Current {s.data[c]["asks"][0][0]}|{s.data[c]["bids"][0][0]}  Base {marg_pos["basePrice"]}\n'
        out_s += f'{color_string(nformat(s.pairs[c]["strike"]), "LIGHT_MAGENTA")}   '
        out_s += f'-{100*buy_target(s, s.active_chunks[c],  s.pairs[c]["streak"]["buy"]):.2f}%|' +\
                 f'+{100*sell_target(s, s.active_chunks[c], s.pairs[c]["streak"]["sell"]):.2f}%  ::  ' +\
                 '|'.join([nformat(v) for v in s.pairs[c]["target"].values()]) + "\n"
    
    return out_s


conf = {
    "base_currency": "BTC",
    "chunk_size": 0.0105,
    "trade_type": "margin",
    "api_access": True,
    "save": ["pairs"],
    
    # on_init runs when the bot is created, even before it is started
    "on_init": on_init,
    # on_start runs when the bot starts running
    "on_start": on_start,
    # on_update runs in the main update loop
    "on_update": on_update,
    # status_message returns a string to be displayed in the terminal on update
    "status_message": status_message
}
