import unittest
from unittest.mock import patch
from poloniex_bot import Bot

# Possible configs, <name: default value>
saved_cfgs = {
    "strategy": None,
    "name": None,
    "base_currency": "BTC",
    "currencies": [],
    "chunk_size": None,
    "chunks": 10,
    "trade_type": "spot",
    "save": [],
    
    "on_init": None,
    "on_start": None,
    "on_update": None,
    "on_close": None,
    "status_message": None,
}


class MockManager():

    def __init__(self):
        self.api = None

    def request_action(self):
        pass


class FirstTest(unittest.TestCase):
    def setUp(self):
        self.bot = Bot(self.mock_manager, {"ignore_saved_status":True})
        self.mock_manager = MockManager()

    def test_initialize_bot(self):
        self.assertTrue(self.bot)
        for k,v in saved_cfgs.items():
            self.bot.getattr(k)



    def test_status(self):
        # Fields saved to bot.status. <name:expected value>
        status_fields = {
            "active": False,
            "currency_pairs": ["BASE_CUR1", "BASE_CUR2"],
            "api": None,
            "manager": self.mock_manager,
            "chunk_size" 
        }



if __name__ == "__main__":
    unittest.main()
