import unittest
from helpers import *

class TestHelpers(unittest.TestCase):
    def test_color_string(self):
        # Can't automatically test that colors match their names so let's 
        # print it
        print()
        print("Text colors")
        colors = ["red", "green", "yellow", "blue", "magenta", "cyan", "white"]
        color_str = ""
        for color in colors:
            color_str += color_string(color + " " * (10 - len(color)), color)
            color_str += color_string("light_" + color, "light_" + color) + "\n"
        
        print(color_str.strip())

        self.assertEqual(
            color_string("hello", "ReD"),
            "\u001b[31mhello\u001b[0m"
        )

    def test_line_end(self):
        print()
        print("Line end")
        s = "This is the previous line!!!!!!!\rLine ends here ->"
        print(s)
        print(s + LINE_END)
        self.assertEqual(LINE_END, "\033[K\n")
    
    def test_nformat(self):
        # unaffected by rounding errors
        self.assertEqual(nformat(0.1 + 0.2), "0.30000000")
        # works with integers
        self.assertEqual(nformat(1), "1.00000000")
        # rounds correctly
        self.assertEqual(nformat(0.000000035), "0.00000004")
        self.assertEqual(nformat(0.0000000349), "0.00000003")
    
    def test_read_conf(self):
        test_conf = {
            "first": "hello",
            "second": lambda x: x
        }

        # reads config value
        self.assertEqual(read_conf(test_conf, "first", ""), "hello")
        # sets default value
        self.assertEqual(read_conf(test_conf, "no", "wow"), "wow")
        # can save functions
        self.assertTrue(callable(read_conf(test_conf, "second", "")))