import unittest
import os
from shutil import rmtree
from types import SimpleNamespace
import json

from helpers import *
from logger import Logger

TMP_DIR = "test_tmp"

class MockBot:
    def __init__(self):
        self.name = "testbot"
        self.status = SimpleNamespace()
        self.to_save = ["hello"]
        self.currencies = ["BTC"]

        self.status.some_value = 5
        self.status.hello = "hi"

        self.status_message = lambda _: self.name + "\n"

class MockManager:
    def __init__(self,bots):
        self.status_message = lambda _: "mng\n"
        self.bots = bots

class TestLogger(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        os.mkdir(TMP_DIR)

    @classmethod
    def tearDownClass(cls):
        rmtree(TMP_DIR)

    def setUp(self):
        self.bot = MockBot()
        self.logger = Logger(self.bot, TMP_DIR)

    def test_save_status(self):
        self.logger.save_status()

        with open(f"{TMP_DIR}/testbot.json", "r") as f:
            saved = json.load(f)

        self.assertEqual(saved["to_save"], self.bot.to_save)
        self.assertTrue("hello" in saved["status"])
        self.assertTrue("some_value" not in saved["status"])
        self.assertEqual(saved["status"]["hello"], "hi")

    def test_load_status(self):
        # Add something else to save
        self.bot.to_save.append("some_value")
        self.logger.save_status()

        # Empty out bot status
        self.bot.status = SimpleNamespace()
        self.assertTrue("hello" not in self.bot.status.__dict__)

        self.logger.load_status()
        self.assertTrue("hello" in self.bot.status.__dict__)
        self.assertEqual(self.bot.status.hello, "hi")
        self.assertEqual(self.bot.status.some_value, 5)

    def test_generate_status(self):
        bot2 = MockBot()
        bot2.name = "testbot2"
        mng = MockManager([self.bot, bot2])
        self.logger.generate_status(mng, True)

        with open(f"{TMP_DIR}/status.txt", "r") as f:
            message = f.read()
        self.assertEqual(message, "mng\ntestbot\ntestbot2\n")





