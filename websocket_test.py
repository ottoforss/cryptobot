import websocket

import time
import urllib
import json
import config
import hmac, hashlib

cred = config.credentials


nonce = 'nonce=' + str(int(time.time()*1000))
post_data = bytes(nonce, 'utf-8')

key = cred["api_key"]
sign = hmac.new(bytearray(cred["secret"], "utf-8"), post_data, hashlib.sha512).hexdigest()

req = {
    'payload': nonce, 
    'command': "subscribe",
    'channel': 1000,
    'sign': sign,
    'key': key    
}
req2 = {
    'command': "subscribe",
    'channel': 1001,
}


print(json.dumps(req))
ws = websocket.WebSocket()
ws.connect("wss://api2.poloniex.com")
ws.send(json.dumps(req))
ws.send(json.dumps(req2))
while True:
    r = ws.recv()
    if r != "[1010]":
        print(r)



# ws = websocket.WebSocketApp("wss://api2.poloniex.com", header=headers)
